## Babelnet related resources

 - **SPARQL Endpoint**: https://babelnet.org/sparql/
 - **Linked data faceted browser**: http://babelnet.org/rdf/page/
 - **Babelfy interface:**: http://babelfy.org/
 - **API-key**: c211260c-33fa-456e-9ea8-536e47575a76
 - **Rest API Specification**: http://babelfy.org/guide