class TedTalk:
    def __init__(self, file_name='', lan='', url='', pagesize='', dtime='', encoding='', content_type='',
                 keywords=None,
                 speaker=None,
                 talkid='',
                 videourl='',
                 videopath='',
                 date='',
                 title='',
                 description='',
                 seekvideo=None,
                 translator=None,
                 reviewer=None,
                 wordnum='',
                 charnum='',
                 content=''):
        self.url = url
        if reviewer is None:
            reviewer = {}
        if translator is None:
            translator = {}
        if seekvideo is None:
            seekvideo = {}
        if keywords is None:
            keywords = []
        self.keywords = keywords
        self.file_name = file_name
        self.lan = lan
        self.pagesize = pagesize
        self.dtime = dtime
        self.encoding = encoding
        self.content_type = content_type
        self.speaker = speaker
        self.talkid = talkid
        self.videourl = videourl
        self.videopath = videopath
        self.date = date
        self.title = title
        self.description = description
        self.seekvideo = seekvideo
        self.translator = translator
        self.reviewer = reviewer
        self.wordnum = wordnum
        self.charnum = charnum
        self.content = content
