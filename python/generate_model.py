import uuid

import rdflib
from rdflib import URIRef, Graph, Literal
from rdflib.namespace import NamespaceManager, OWL, Namespace, RDF, RDFS

import babelnet


class TedLLODURIGenerator:
    def __init__(self, base_uri):
        self.base_uri = base_uri
        self._prefix = rdflib.Namespace(base_uri)
        self._ted_pattern = "tedlod:"

    # def create_xxx_uri_uuid(self, uniquely_identifying_values):
    #     uuid_key = uniquely_identifying_values
    #     return URIRef(self._prefix["some_pattern/" + str(uuid.uuid5(namespace=uuid.NAMESPACE_URL, name=uuid_key))])

    def create_catalog(self) -> URIRef:
        return URIRef(self._prefix['ted_talks_catalogue'])

    def create_dataset_id(self, id):
        return URIRef(self._prefix["dataset_{0}".format(str(id))])

#    def create_dataset_id(self, id):
#        return URIRef(self._prefix["{0}dataset_{1}".format(self._ted_pattern, str(id))])

    """
    def create_ted_talk_id(self, id):
        return URIRef(self._prefix["{0}talk_{1}".format(self._ted_pattern, str(id))])

    def create_ted_transcription_id(self, id):
        return URIRef(self._prefix["{0}transcription_{1}".format(self._ted_pattern, str(id))])

    def create_ted_segment_seek_id(self, seek_id):
        return URIRef(self._prefix["{0}segment_{1}".format(self._ted_pattern, str(seek_id))])
    """
    def create_catalog_person(self):
        return URIRef(self._prefix['publisher'])

    def create_video_object_id(self, talk_id):
        return URIRef(self._prefix['videoobject'])

    def create_uri_explicit(self, uniquely_identifying_value):
        return URIRef(self._prefix["sometype/" + uniquely_identifying_value])

    def annotation_uri(self, begin, end, text, ref, confidence, source_text_content):
        uuid_key = str(begin) + str(end) + str(text) + str(ref) + str(round(confidence, 2)) + source_text_content
        return URIRef(
            self._prefix["annotation/" + str(uuid.uuid5(namespace=uuid.NAMESPACE_URL, name=uuid_key))])

    def create_distribution_id(self, talk_id, language):
        return URIRef(self._prefix['distribution_{0}_{1}'.format(str(talk_id), language)])

    def create_context_id_language(self, talk_id, language):
        return URIRef(self._prefix['transcription_{0}_{1}'.format(talk_id, language)])

    def create_segment_uri(self, talk_id, language, seekvideo_id, begin_index, end_index):
        return URIRef(self._prefix[
                          'segment_{0}_{1}_{2}#char={3},{4}'.format(talk_id, language, seekvideo_id, begin_index,
                                                                       end_index)])
#                          'segment_{0}_{1}{2}#char=({3}),({4})'.format(talk_id, language, seekvideo_id, begin_index,
#                                                                       end_index)])


    def create_video_seek_time(self, talk_id, seekvideo_id):
        return URIRef(self._prefix['videoseektime_{0}_{1}'.format(talk_id, seekvideo_id)])

    def create_person_id(self, speaker):
        return URIRef(self._prefix['speaker_{0}'.format(speaker.replace(' ', '_').lower())])


class TedLLODGenerator:
    def __init__(self, model_uri, threshold=0.3):
        self._graph = rdflib.Graph()

        self._uri_generator = TedLLODURIGenerator(model_uri)
        self._threshold = threshold

        self.model_uri = model_uri
        self._namespace_manager = NamespaceManager(Graph())

        self._tedlod_prefix = rdflib.Namespace(model_uri)
        self._namespace_manager.bind('tedlod', self._tedlod_prefix, override=False)
        #self._namespace_manager.bind('http://purl.org/tedlod/', self._tedlod_prefix, override=True)

        self._tedlod_nif_transcription = URIRef(self._tedlod_prefix['nifTranscription'])
        self._graph.add((self._tedlod_nif_transcription, RDFS.subPropertyOf, RDF.Property))

        self._rdfs_prefix = rdflib.Namespace("http://www.w3.org/2000/01/rdf-schema#")
        self._namespace_manager.bind('rdfs', self._rdfs_prefix, override=False)

        self._schema_prefix = rdflib.Namespace("http://schema.org/")
        self._namespace_manager.bind('schema', self._schema_prefix, override=False)

        self._namespace_manager.bind('owl', OWL, override=True)

        self._dbo_prefix = rdflib.Namespace("http://dbpedia.org/ontology/")
        self._namespace_manager.bind("dbo", self._dbo_prefix, override=False)

        self._dbr_prefix = rdflib.Namespace("http://dbpedia.org/resource/")
        self._namespace_manager.bind("dbr", self._dbr_prefix, override=False)

        self._dcat_prefix = rdflib.Namespace("http://www.w3.org/ns/dcat#")
        self._namespace_manager.bind("dcat", self._dcat_prefix, override=False)

        self._dct_prefix = rdflib.Namespace("http://purl.org/dc/terms/")
        self._namespace_manager.bind("dct", self._dct_prefix, override=False)

        self._foaf_prefix = rdflib.Namespace("http://xmlns.com/foaf/0.1/")
        self._namespace_manager.bind("foaf", self._foaf_prefix, override=False)

        self._vcard_prefix = rdflib.Namespace("http://www.w3.org/2006/vcard/ns#")
        self._namespace_manager.bind("vcard", self._vcard_prefix, override=False)

        self._adms_prefix = Namespace("http://www.w3.org/ns/adms#")
        self._namespace_manager.bind("adms", self._adms_prefix, override=False)

        self._skos_prefix = Namespace("http://www.w3.org/2004/02/skos/core#")
        self._namespace_manager.bind("skos", self._skos_prefix, override=False)

        self._prov_prefix = Namespace("http://www.w3.org/ns/prov#")
        self._namespace_manager.bind("prov", self._prov_prefix, override=False)

        self._owl_same_as = URIRef(OWL['sameAs'])

        self._dcat_Catalog = URIRef(self._dct_prefix['Catalog'])
        self._dcat_Dataset = URIRef(self._dcat_prefix['Dataset'])
        self._dcat_Distribution = URIRef(self._dcat_prefix['Distribution'])
        self._dcat_keywords = URIRef(self._dcat_prefix['keywords'])
        self._dcat_landing_page = URIRef(self._dcat_prefix['landingPage'])
        self._dcat_access_url = URIRef(self._dcat_prefix['accessURL'])
        self._dcat_download_url = URIRef(self._dcat_prefix['downloadURL'])
        self._dcat_distribution = URIRef(self._dcat_prefix['distribution'])
        self._dcat_mediaType = URIRef(self._dcat_prefix['mediaType'])

        self._dct_identifier = URIRef(self._dct_prefix['identifier'])
        self.dct_issued = URIRef(self._dct_prefix['issued'])
        self._dct_title = URIRef(self._dct_prefix['title'])
        self._dct_description = URIRef(self._dct_prefix['description'])
        self._dct_language = URIRef(self._dct_prefix['language'])
        self._dct_has_part = URIRef(self._dct_prefix['hasPart'])
        self.dct_creator = URIRef(self._dct_prefix['creator'])
        self.dct_license = URIRef(self._dct_prefix['license'])
        self._dct_publisher = URIRef(self._dct_prefix['publisher'])

        self._schema_VideoObject = URIRef(self._schema_prefix['VideoObject'])
        self.schema_CreativeWork = URIRef(self._schema_prefix['CreativeWork'])
        self._schema_Person_class_uri = URIRef(self._schema_prefix['Person'])
        self._schema_url_property_uri = URIRef(self._schema_prefix['url'])
        self._schema_date_published_property_uri = URIRef(self._schema_prefix['datePublished'])
        self._schema_in_language_preperty_uri = URIRef(self._schema_prefix['inLanguage'])
        self._schema_author_property_uri = URIRef(self._schema_prefix['author'])
        self._schema_description_property_uri = URIRef(self._schema_prefix['description'])
        self._schema_mentions_property_uri = URIRef(self._schema_prefix['mentions'])
        self._schema_keywords_property_uri = URIRef(self._schema_prefix['keywords'])
        self._schema_text_property_uri = URIRef(self._schema_prefix['text'])
        self._schema_image = URIRef(self._schema_prefix['image'])
        self._schema_content_url = URIRef(self._schema_prefix['contentUrl'])
        self._schema_date_created = URIRef(self._schema_prefix['dateCreated'])
        self._schema_video = URIRef(self._schema_prefix['video'])

        self._nif_prefix = rdflib.Namespace("http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#")
        self._namespace_manager.bind('nif', self._nif_prefix, override=False)

        self._nif_RFC5147String_class_uri = URIRef(self._nif_prefix['RFC5147String'])
        self._nif_context_class_uri = URIRef(self._nif_prefix['Context'])

        self._nif_source_url_property_uri = URIRef(self._nif_prefix['sourceUrl'])
        self._nif_begin_index_property_uri = URIRef(self._nif_prefix["beginIndex"])
        self._nif_end_index_property_uri = URIRef(self._nif_prefix["endIndex"])
        self._nif_is_string_property_uri = URIRef(self._nif_prefix["isString"])
        self._nif_substring_property = URIRef(self._nif_prefix['subString'])

        self._its_prefix = rdflib.Namespace("https://www.w3.org/2005/11/its/rdf#")
        self._namespace_manager.bind('itsrdf', self._its_prefix, override=False)

        self.its_ta_confidence_property_uri = URIRef(self._its_prefix['taConfidence'])
        self.its_ta_ident_ref_property_uri = URIRef(self._its_prefix['taIdentRef'])

        self._foaf_homepage = URIRef(self._foaf_prefix['homepage'])
        self._foaf_person = URIRef(self._foaf_prefix['Person'])
        self._foaf_firstName = URIRef(self._foaf_prefix['firstName'])
        self._foaf_lastName = URIRef(self._foaf_prefix['lastName'])

        self._datcat_dc2564 = URIRef("www.isocat.org/datcat/DC-2564")

        self._prov_person_class_uri = URIRef(self._prov_prefix['Person'])
        self._prov_was_attributed_to_property_uri = URIRef(self._prov_prefix['wasAttributedTo'])
        self._prov_had_primary_source_property_uri = URIRef(self._prov_prefix['hadPrimarySource'])

        self._foaf_name = URIRef(self._foaf_prefix['name'])

        self._tedlod_has_video_seek_time_property_uri = URIRef(self._tedlod_prefix['hasVideoSeektime'])

        self._rdfs_value_property_uri = URIRef(self._rdfs_prefix['value'])
        self._rdfs_Class = URIRef(self._rdfs_prefix['Class'])
        self._rdfs_subClassOf_property_uri = URIRef(self._rdfs_prefix['subClassOf'])

    def generate_model(self, data):
        row_counter = 0

        self._graph.namespace_manager = self._namespace_manager

        # Catalog
        catalog_uri = self._uri_generator.create_catalog()
        self._graph.add((catalog_uri, RDF.type, self._dcat_Catalog))
        self._graph.add((catalog_uri, self._foaf_homepage, URIRef("https://wit3.fbk.eu/")))
        self._graph.add((catalog_uri, self.dct_license, URIRef("https://creativecommons.org/ns#cc_by_nc_nd3.0")))

        # Person
        publisher_person_uri = self._uri_generator.create_catalog_person()
        self._graph.add((catalog_uri, self._dct_publisher, publisher_person_uri))
        self._graph.add((publisher_person_uri, RDF.type, self._tedlod_prefix['']))

        # TODO: make static constants out of first and lastname
        self._graph.add((publisher_person_uri, self._foaf_firstName, Literal("Christian")))
        self._graph.add((publisher_person_uri, self._foaf_firstName, Literal("Girandi")))

        already_processed_talk_ids = []
        different_talks_counter = 0
        talks_counter = 0
        for language, talks in data.items():
            for talk in talks:
                talk_id = talk.talkid
                if talk_id not in already_processed_talk_ids:
                    different_talks_counter = different_talks_counter + 1

                already_processed_talk_ids.append(talk_id)
                talks_counter = talks_counter + 1
                # Dataset
                dataset_uri = self._uri_generator.create_dataset_id(talk_id)
                self._graph.add((catalog_uri, self._dct_has_part, dataset_uri))
                self._graph.add((dataset_uri, RDF.type, self._dcat_Dataset))
                self._graph.add((dataset_uri, RDF.type, self.schema_CreativeWork))
                self._graph.add((dataset_uri, self._dct_identifier, Literal(talk_id)))
                for keyword in talk.keywords:
                    self._graph.add((dataset_uri, self._dcat_keywords, Literal(keyword)))
                # self._graph.add((dataset_uri, self._schema_image, URIRef(talk.image)))
                self._graph.add((dataset_uri, self.dct_issued, Literal(talk.dtime)))
                self._graph.add((dataset_uri, self._dcat_landing_page, URIRef(talk.url)))

                # VideoObject
                video_object_uri = self._uri_generator.create_video_object_id(talk_id)
                self._graph.add((dataset_uri, self._schema_video, video_object_uri))
                self._graph.add((video_object_uri, RDF.type, self._schema_VideoObject))
                self._graph.add((video_object_uri, self._schema_date_created, Literal(talk.date)))

                speaker_uri = self._uri_generator.create_person_id(talk.speaker)
                self._graph.add((video_object_uri, self._schema_author_property_uri, speaker_uri))

                # TODO: for future work use schema_name
                self._graph.add((speaker_uri, self._foaf_name, Literal(talk.speaker)))
                # BabelFy Speaker Query
                queryGenerator = babelnet.BabelFyQueryGenerator()
                response = queryGenerator.create_babel_fy_query(str(talk.speaker)).run_query()
                data = babelnet.process_response(response)
                entities = []
                for result in data:
                    # retrieving char fragment
                    charFragment = result.get('charFragment')
                    cfStart = charFragment.get('start')
                    cfEnd = charFragment.get('end')

                    dbpediaUrl = result.get('DBpediaURL')
                    babelNetUrl = result.get('BabelNetURL')

                    globalScore = result.get('globalScore')
                    entities.append({'Char_Offset:': str(cfStart) + "-" + str(cfEnd),
                                     'DBpediaURL': dbpediaUrl,
                                     'BabelNetURL': babelNetUrl})

                    self._graph.add((speaker_uri, self._its_prefix['taIdentRef'], URIRef(dbpediaUrl)))

                # Distribution
                distribution_uri = self._uri_generator.create_distribution_id(talk_id, language)
                self._graph.add((dataset_uri, self._dcat_distribution, distribution_uri))
                self._graph.add((distribution_uri, RDF.type, self._dcat_Distribution))
                self._graph.add((distribution_uri, self._dct_title, Literal(talk.title)))
                self._graph.add((distribution_uri, self._dct_description, Literal(talk.description)))
                self._graph.add((distribution_uri, self._dcat_access_url,
                                 URIRef("https://www.wit3.fbk.eu/mono.php?release=XML_releases&tinfo=cleanedhtml_ted")))
                self._graph.add((distribution_uri, self._dct_language,
                                 URIRef("https://www.lexvo.org/id/iso639_1/{0}".format(language))))
                self._graph.add((distribution_uri, self._datcat_dc2564, Literal(talk.encoding)))
                self._graph.add((distribution_uri, self._dcat_download_url, URIRef(
                    "https://www.wit3.fbk.eu/download.php?release=XML_releases/xml&type=zip&slang=ted_{0}-20160408.zip&tlang=undefined".format(
                        language))))
                self._graph.add((distribution_uri, self._dcat_mediaType, Literal(talk.content_type)))

                # Context
                context_uri = self._uri_generator.create_context_id_language(talk_id, language)
                self._graph.add((distribution_uri, self._tedlod_nif_transcription, self._nif_context_class_uri))
                self._graph.add((context_uri, self._nif_is_string_property_uri, Literal(talk.content)))
                self._graph.add((context_uri, self._nif_begin_index_property_uri, Literal("0")))
                self._graph.add((context_uri, self._nif_end_index_property_uri, Literal(str(len(talk.content)))))

                # String
                begin_index = 0
                end_index = 0
                for seekvideo_id, string in talk.seekvideo.items():
                    end_index = begin_index + len(string)
                    string_uri = self._uri_generator.create_segment_uri(talk_id, language, seekvideo_id, begin_index,
                                                                        end_index)
                    self._graph.add((context_uri, self._nif_substring_property, string_uri))
                    begin_index = end_index

                    # VideoSeekTime
                    video_seek_time_uri = self._uri_generator.create_video_seek_time(talk_id, seekvideo_id)
                    self._graph.add((string_uri, self._tedlod_has_video_seek_time_property_uri, video_seek_time_uri))
                    self._graph.add((video_seek_time_uri, self._rdfs_value_property_uri, Literal(seekvideo_id)))
                    self._graph.add((video_seek_time_uri, self._rdfs_subClassOf_property_uri, self._rdfs_Class))

                # Person
                self._graph.add((context_uri, self._prov_was_attributed_to_property_uri, self._prov_person_class_uri))
                self._graph.add(
                    (self._prov_person_class_uri, self._prov_had_primary_source_property_uri, URIRef(talk.videourl)))
                for name, homepage in talk.translator.items():
                    self._graph.add((self._prov_person_class_uri, self._foaf_name, Literal(name)))
                    self._graph.add((self._prov_person_class_uri, self._foaf_homepage, URIRef(homepage)))

        self._graph.serialize('./talk.turtle'.format(language), format='turtle')
        print(different_talks_counter)
        print(talks_counter)
