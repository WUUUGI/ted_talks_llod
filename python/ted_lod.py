import babelnet
from generate_model import TedLLODURIGenerator, TedLLODGenerator

# TODO: read in files with parser
import parse

data = parse.parse('../dataset/')
generator = TedLLODGenerator("https://www.purl.org/tedlod/")
generator.generate_model(data)

# TODO: query babelnet
