import gzip
import io
import json
import urllib.parse
import urllib.request


def create_query_params(text, lang, key):
    return {
        'text': text,
        'lang': lang,
        'key': key
    }


def process_response(response):
    if response.info().get('Content-Encoding') == 'gzip':
        buf = io.BytesIO(initial_bytes=response.read())
        f = gzip.GzipFile(fileobj=buf)
        return json.loads(f.read())


class BabelFyQueryGenerator:
    def __init__(self, lang='EN', key='c211260c-33fa-456e-9ea8-536e47575a76'):
        self._service_url_ = 'https://babelfy.io/v1/disambiguate'
        self._lang_ = lang
        self._key_ = key

    def create_babel_fy_query(self, text):
        params = create_query_params(text, self._lang_, self._key_)
        return BabelFyQuery(self._service_url_ + '?' + urllib.parse.urlencode(params))


class BabelFyQuery:
    def __init__(self, url):
        self._url_ = url

    def run_query(self):
        request = urllib.request.Request(self._url_)
        request.add_header('Accept-encoding', 'gzip')
        return urllib.request.urlopen(request)