#this file parse all xml files in a folder 
#the parameter for parse function is the folder path 


import xml.etree.ElementTree as ET
from TedTalk import TedTalk  
import os
adic={}

def parse(path):
  #********PATH TO THE FODER**************************#
  #***********READ ALL XML FILES IN A FOLDER**********#
  for filename in os.listdir(path):
    if not filename.endswith('.xml'): continue
    fullname = os.path.join(path, filename)
    #print(fullname)
    tree = ET.parse(fullname)
    root = tree.getroot()
    #print(root.tag)
    lan=root.attrib['language']
    #print(lan)
    adic[lan]=[]
    for f in root.findall('file'):
      #print(f.attrib['id'])
      pobject=TedTalk()
      pobject.file_name=filename
      pobject.lan=lan
      url = f.find('head').find('url').text
      pobject.url=url
      pagesize = f.find('head').find('pagesize').text
      pobject.pagesize=pagesize
      dtime = f.find('head').find('dtime').text
      pobject.dtime=dtime
      encoding = f.find('head').find('encoding').text
      pobject.encoding=encoding
      content_type = f.find('head').find('content-type').text
      pobject.content_type=content_type
      keywords = f.find('head').find('keywords').text
      pobject.keywords=keywords.split(',')
      adic[lan].append(pobject)
      speaker = f.find('head').find('speaker').text
      pobject.speaker=speaker      
      talkid = f.find('head').find('talkid').text
      pobject.talkid=talkid
      videourl = f.find('head').find('videourl').text
      pobject.videourl=videourl
      videopath = f.find('head').find('videopath').text
      pobject.videopath=videopath
      date = f.find('head').find('date').text
      pobject.date=date
      title = f.find('head').find('title').text
      pobject.title=title
      encoding = f.find('head').find('description').text
      pobject.encoding=encoding
      description = f.find('head').find('encoding').text
      pobject.description=description
      wordnum = f.find('head').find('wordnum').text
      pobject.wordnum=wordnum
      charnum = f.find('head').find('charnum').text
      pobject.charnum=charnum

      content = f.find('content').text
      pobject.content=content
      
      seekvideo_dic={}
      for m in f.find('head').find('transcription').findall('seekvideo'):
        seekvideo_dic[m.attrib['id']] = m.text
        #print(m.text)
        #print(m.attrib['id'])

      pobject.seekvideo=seekvideo_dic

      translators_dic={}
      if f.find('head').find('translators') is not None:
        for m in f.find('head').find('translators').findall('translator'):
          translators_dic[m.text] = m.attrib['href']
      pobject.translator=translators_dic
    
      reviewer_dic={}
      if f.find('head').find('reviewers') is not None:
        for m in f.find('head').find('reviewers').findall('reviewer'):
          reviewer_dic[m.text] = m.attrib['href']
      pobject.reviewer=reviewer_dic

      #print(url)
  return adic


##call the function with path as a parameter
#ob=parse('/var/www/html/data2/summer_school/')

