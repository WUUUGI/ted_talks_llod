import gzip
import io
import json
import urllib.parse
import urllib.request


def create_query_params(text, lang, key):
    return {
        'text': text,
        'lang': lang,
        'gcube-token': key
    }


def process_response(response):
    if response.info().get('Content-Encoding') == 'gzip':
        buf = io.BytesIO(initial_bytes=response.read())
        f = gzip.GzipFile(fileobj=buf)
        return json.loads(f.read())


class TagMeQueryGenerator:
    def __init__(self, lang='EN', key='2adffc3e-7795-40df-b8c3-0d5afa892020-843339462'):
        self._service_url_ = 'https://tagme.d4science.org/tagme/tag'
        self._lang_ = lang
        self._key_ = key

    def create_babel_fy_query(self, text):
        params = create_query_params(text, self._lang_, self._key_)
        return TagMeQueryGenerator(self._service_url_ + '?' + urllib.parse.urlencode(params))


class TagMeQuery:
    def __init__(self, url):
        self._url_ = url

    def run_query(self):
        request = urllib.request.Request(self._url_)
        request.add_header('Accept-encoding', 'gzip')
        return urllib.request.urlopen(request)
