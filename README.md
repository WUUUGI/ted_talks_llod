# Ted_talks_LLOD: TED Talks Transformation to LOD

## Description

We convert transcript files of TED talks from XML into RDF. We provide the code for generating the triples and BabelNet annotation.

## Overview 

- dataset directory: contains sample XML files of the TED talks
- yupiter directory: contains the Python code for the triple generation and BabelNet annotation
- URIs.txt defines the structure of the URIs we use in our model
- UseCases.txt contains ideas for the usage of the generated RDF resource
- mapping.csv contains the mapping of the XML tags to RDF vocabularies
- tasks.txt describes the procedure of conducting the project
- TEDLOD_diagram.jpg and TEDLOD_diagram.pdf: shows the diagram of our RDF model


## Data

The XML files of the TED talks transcripts as part of this project can be obtained from [https://wit3.fbk.eu/](https://wit3.fbk.eu/)
This corpus is licensed under the CC BY-NC-ND license.


## Python Code 

The main interface for the converter is the **ted_lod.py** that parses input documents from the source corpus in *../dataset/*, and generates an RDF model (turtle file) for each of them (one file per language). The script can be run from anywhere provided that the *../dataset/* directory contains the source corpus. 

- **TedTalks.py** contains the TedTalk python class. The class represents a single TedTalk XML document and consists of information, relevant to the talk, stored in the XML document. In “Information from the XML files” exact details can be found about the used data.
- **parse.py** returns a dictionary object which contains a language code as a key and an array of TedTalk objects as a value. Each TedTalk object corresponding to one xml document. 
- **generate_model.py** performs the actual population of the RDF dataset on the basis of the information extracted during the parsing step into a list of TedTalk instances. 
- **babelnet.py** is an utility script that offers a function that takes text as input and returns the annotations through the Babelnet API as json dictionaries. The entity annotations are used to annotate the transcripts. 
